FROM python:3.7-alpine

ADD /bin/run.sh /bin/run.sh

COPY dist/ dist/

RUN apk add libcrypto1.0 && \
    ln -s /usr/lib/libcrypto.so.1.0.0 /usr/lib/libcrypto.so && \
    chmod +x /bin/run.sh && \
    pip install dist/*.whl && \
    rm -rf dist/

CMD ["run.sh"]
