import unittest
import logging
from unittest import mock
from argparse import ArgumentParser
from rundeck_resources.cli import main
from rundeck_resources.cli import argument_parse
from rundeck_resources.cli import load_importers
from rundeck_resources.cli import load_exporters
from rundeck_resources.cli import import_resources
from rundeck_resources.cli import export_resources
from rundeck_resources.cli import initialize_import_interfaces
from rundeck_resources.cli import initialize_export_interfaces
from rundeck_resources.cli import verbosity
from rundeck_resources.errors import ConfigError
from rundeck_resources.errors import NoResourcesFound
from rundeck_resources.interfaces import ResourcesImporter
from rundeck_resources.interfaces import ResourcesExporter


class MockImportInterface(ResourcesImporter):
    def __init__(self, title, config, cache):
        self.title = title
        self.config = config
        self.cache = cache

    def import_resources(self):
        return {'result': True}


class MockExportInterface(ResourcesExporter):
    def __init__(self, title, config):
        self.title = title
        self.config = config

    def export_resources(self, resources):
        pass


class MockArgumentParser:
    def __init__(self):
        self.config = {}
        self.verbose = 0
        self.no_cache = False
        self.logger = None

    def parse_args(self):
        return self


def mock_cache(config=None, no_cache=False):
    return config


def mock_read_config(config):
    if config == {}:
        return config
    return None


def mock_load_plugin(config, plugin):
    return config[plugin]


class TestCLI(unittest.TestCase):
    @mock.patch('rundeck_resources.cli.export_resources')
    @mock.patch('rundeck_resources.cli.import_resources')
    @mock.patch('rundeck_resources.cli.initialize_export_interfaces')
    @mock.patch('rundeck_resources.cli.initialize_import_interfaces')
    @mock.patch('rundeck_resources.cli.load_exporters')
    @mock.patch('rundeck_resources.cli.load_importers')
    @mock.patch('rundeck_resources.cli.Cache',
                side_effect=mock_cache)
    @mock.patch('rundeck_resources.cli.read_config',
                side_effect=mock_read_config)
    @mock.patch('rundeck_resources.cli.argument_parse',
                side_effect=MockArgumentParser)
    def test_main(self, mock_ap, mock_rc, mock_c,
                  mock_li, mock_le, mock_iii,
                  mock_iei, mock_ir, mock_er):
        main()
        config = {}
        assert mock_ap.called
        mock_ap.assert_called_with()
        assert mock_rc.called
        mock_rc.assert_called_with(config)
        assert mock_c.called
        mock_c.assert_called_with(config, False)
        assert mock_li.called
        mock_li.assert_called_with(config)
        assert mock_le.called
        mock_le.assert_called_with(config)
        assert mock_iii.called
        mock_iii.assert_called_with(config, mock_li(), mock_c(config))
        assert mock_iei.called
        mock_iei.assert_called_with(config, mock_le())
        assert mock_ir.called
        mock_ir.assert_called_with(mock_iii())
        assert mock_er.called
        mock_er.assert_called_with(mock_iei(), mock_ir.return_value)

        # Test NoResourcesFound
        mock_ir.side_effect = NoResourcesFound
        self.assertRaises(SystemExit, main)

        # Test ConfigError on Exporters
        mock_le.side_effect = ConfigError
        self.assertRaises(SystemExit, main)

        # Test ConfigError on Importers
        mock_li.side_effect = ConfigError
        self.assertRaises(SystemExit, main)

    @mock.patch('rundeck_resources.cli.load_plugins')
    def test_load_importer(self, mock_lp):
        config = {'Importers': {}}
        mock_lp.side_effect = mock_load_plugin
        self.assertEqual(load_importers(config), {})

    @mock.patch('rundeck_resources.cli.load_plugins')
    def test_load_exporter(self, mock_lp):
        config = {'Exporters': {}}
        mock_lp.side_effect = mock_load_plugin
        self.assertEqual(load_exporters(config), {})

    def test_initialize_import_interfaces(self):
        config = {}
        plugins = {
            'TestPlugin': {
                'plugin': MockImportInterface,
                'title': 'TestPlugin'}}
        cache = mock_cache()
        result = initialize_import_interfaces(config, plugins, cache)
        self.assertIsInstance(result[0], MockImportInterface)
        self.assertEqual(result[0].config, config)

    def test_initialize_export_interfaces(self):
        config = {}
        plugins = {
            'TestPlugin': {
                'plugin': MockExportInterface,
                'title': 'TestPlugin'}}
        result = initialize_export_interfaces(config, plugins)
        self.assertIsInstance(result[0], MockExportInterface)
        self.assertEqual(result[0].config, config)

    def test_import_resources(self):
        interfaces = [MockImportInterface('TestPlugin', {}, mock_cache())]
        result = import_resources(interfaces)
        expected_result = {'result': True}
        self.assertEqual(result, expected_result)
        self.assertRaises(NoResourcesFound,
                          import_resources, [])

    def test_export_resources(self):
        interfaces = [MockExportInterface('TestPlugin', {})]
        resources = {}
        result = export_resources(interfaces, resources)
        self.assertEqual(result, None)

    def test_argument_parse(self):
        result = argument_parse()
        self.assertIsInstance(result, ArgumentParser)

    def test_verbosity(self):
        self.assertEqual(verbosity(0), logging.ERROR)
        self.assertEqual(verbosity(1), logging.WARNING)
        self.assertEqual(verbosity(2), logging.INFO)
        self.assertEqual(verbosity(3), logging.DEBUG)
        self.assertEqual(verbosity(4), logging.DEBUG)
