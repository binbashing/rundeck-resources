import unittest
from rundeck_resources import config


class TestConfig(unittest.TestCase):
    def test_read_config(self):
        path = 'config/example.ini'
        result = config.read_config(path)
        expected_result = \
            {'ChefImporter:ChefServerName':
             {'chef_version': '12.10.24',
              'rundeck_user_login': 'rundeck',
              'ssl_cert_path': '~/.chef/trusted_certs/api_chef_io.crt',
              'url': 'https://api.chef.io',
              'user': 'username',
              'user_cert_path': '~/.chef/username.pem'},
                'YAMLExporter:YAMLTitle':
                {'export_path': '/var/rundeck/projects/resources.yaml'}
             }
        self.assertDictEqual(result, expected_result)
