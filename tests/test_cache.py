import json
import unittest
from unittest import mock
from rundeck_resources.cache import Cache
from rundeck_resources.errors import CacheNotFound


class TestCache(unittest.TestCase):
    @mock.patch('rundeck_resources.cache.Cache.load_cache')
    def setUp(self, mock_lc):
        mock_lc.return_value = {
            'rundeck_resources_tempdir': '',
            'plugin': ''
        }
        self.config = {
            'plugin': {}}
        self.cache = Cache(self.config, False)

    @mock.patch('rundeck_resources.cache.Cache._create_cache_tempdir')
    @mock.patch('rundeck_resources.cache.Cache._load_plugin_cache')
    @mock.patch('rundeck_resources.cache.tempfile.gettempdir')
    @mock.patch('rundeck_resources.cache.glob.glob')
    def test_load_cache_true(self, mock_g, mock_gtd, mock_lpc, mock_cct):
        mock_gtd.return_value = '/tmp'
        mock_g.return_value = ['/tmp/rundeck_resources']
        self.cache.load_cache(self.config)
        mock_g.assert_called_with('/tmp/rundeck_resources*')
        assert mock_lpc.called
        mock_lpc.assert_called_with(mock_g.return_value[0],
                                    self.config)
        assert not mock_cct.called

    @mock.patch('rundeck_resources.cache.Cache._create_cache_tempdir')
    @mock.patch('rundeck_resources.cache.Cache._load_plugin_cache')
    @mock.patch('rundeck_resources.cache.tempfile.gettempdir')
    @mock.patch('rundeck_resources.cache.glob.glob')
    def test_load_cache_false(self, mock_g, mock_gtd, mock_lpc, mock_cct):
        # Test no tempdir
        mock_gtd.return_value = []
        mock_g.return_value = []
        self.cache.load_cache(self.config)
        assert not mock_lpc.called
        assert mock_cct.called
        mock_cct.assert_called_with(self.config)
        pass

    def test_translate_to_filename(self):
        plugin_name = "TestPlugin:Title"
        result = "testplugin_title"
        self.assertEqual(Cache.translate_to_filename(
            plugin_name), result)

    @mock.patch('rundeck_resources.cache.tempfile.mkdtemp')
    def test_create_cache_tempdir(self, mock_md):
        mock_md.return_value = "/tmp/rundeck_resources"
        result = self.cache._create_cache_tempdir(self.config)
        mock_md.assert_called_with(prefix='rundeck_resources-')
        expected_result = {
            'rundeck_resources_tempdir': '/tmp/rundeck_resources',
            'plugin': ''}
        self.assertEqual(result, expected_result)

    @mock.patch('rundeck_resources.cache.glob.glob')
    @mock.patch('rundeck_resources.cache.tempfile.mkstemp')
    def test_load_plugin_cache(self, mock_ms, mock_g):
        mock_g.return_value = ['/tmp/rundeck_resources/plugin-']
        rundeck_tempdir = '/tmp/rundeck_resources/'
        result = self.cache._load_plugin_cache(rundeck_tempdir,
                                               self.config)
        expected_result = {
            'rundeck_resources_tempdir': '/tmp/rundeck_resources/',
            'plugin': '/tmp/rundeck_resources/plugin-'}
        self.assertDictEqual(result, expected_result)

        # Test no cache file
        mock_g.return_value = ['']
        rundeck_tempdir = '/tmp/rundeck_resources/'
        result = self.cache._load_plugin_cache(rundeck_tempdir,
                                               self.config)
        expected_result = {
            'rundeck_resources_tempdir': '/tmp/rundeck_resources/',
            'plugin': ''}
        self.assertDictEqual(result, expected_result)

    @mock.patch('rundeck_resources.cache.os.remove')
    def test_invalidate(self, mock_or):
        self.cache.cache_config = {
            'rundeck_resources_tempdir': '/tmp/rundeck_resources/',
            'plugin': '/tmp/rundeck_resources/plugin-'}

        self.cache.invalidate('plugin')
        mock_or.assert_called_with('/tmp/rundeck_resources/plugin-')
        self.assertEqual(self.cache.cache_config['plugin'], '')

        # Testing FileNotFoundError
        mock_or.side_effect = FileNotFoundError
        self.cache.cache_config = {
            'rundeck_resources_tempdir': '/tmp/rundeck_resources/',
            'plugin': '/tmp/rundeck_resources/plugin-'}

        self.cache.invalidate('plugin')
        self.assertEqual(self.cache.cache_config['plugin'], '')
        pass

    @mock.patch('rundeck_resources.cache.Cache.invalidate')
    @mock.patch('rundeck_resources.cache.open')
    @mock.patch('rundeck_resources.cache.tempfile.mkstemp')
    def test_cache(self, mock_mt, mock_o, mock_i):
        self.cache.cache_config = {
            'rundeck_resources_tempdir': '/tmp/rundeck_resources',
            'plugin': '/tmp/rundeck_resources/plugin-'}
        mock_mt.return_value = (0, '/tmp/rundeck_resources/new_file')
        mock_o.side_effect = mock.mock_open()
        self.cache.cache('plugin', {})
        mock_mt.assert_called_with(dir='/tmp/rundeck_resources',
                                   prefix='plugin-')
        mock_i.assert_called_with('plugin')
        self.assertEqual(self.cache.cache_config['plugin'],
                         '/tmp/rundeck_resources/new_file')

    @mock.patch('rundeck_resources.cache.Cache.invalidate')
    @mock.patch('rundeck_resources.cache.open')
    @mock.patch('rundeck_resources.cache.tempfile.mkstemp')
    def test_no_cache_cache(self, mock_mt, mock_o, mock_i):
        cache = Cache(self.config, True)
        cache.cache('plugin', {})
        assert not mock_mt.called
        assert not mock_o.called
        assert not mock_i.called

    @mock.patch('rundeck_resources.cache.open')
    def test_uncache(self, mock_o):
        self.cache.cache_config = {
            'plugin': '/tmp/rundeck_resources/plugin-'}

        mock_o.side_effect = mock.mock_open(
            read_data=json.dumps(self.cache.cache_config))
        result = self.cache.uncache('plugin')
        mock_o.assert_called_with('/tmp/rundeck_resources/plugin-', 'r')
        self.assertEqual(result, self.cache.cache_config)

        self.cache.cache_config = {
            'plugin': ''}

        self.assertRaises(CacheNotFound, self.cache.uncache, 'plugin')

    @mock.patch('rundeck_resources.cache.open')
    def test_no_cache_uncache(self, mock_o):
        cache = Cache(self.config, True)
        result = cache.uncache('plugin')
        assert not mock_o.called
        self.assertEqual(result, {})
